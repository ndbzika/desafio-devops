# Desafio Devops Avanti

## Descrição

Utilizei o terraform para criar a infraestrutura na AWS, o script cria uma VPC, uma subnet, uma instância EC2 e um security group.

Para visular os logs do terraform, veja os arquivos:

- [Terraform plan](terraform-structure/plan-response.txt)
- [Terraform apply](terraform-structure/apply-response.txt)


## Meu processo

1. Criei um arquivo `main.tf` com a configuração da infraestrutura
2. Criei um arquivo `ec2.tf` para definir a instância EC2
3. Criei um arquivo `securiry.group.tf` para definir o security group
4. Executei o comando `terraform init` para inicializar o diretório
6. Executei o comando `terraform plan` para visualizar o que será criado
6. Executei o comando `terraform apply` para criar a infraestrutura
7. Na instância EC2, instalei o gitlab-runner e registrei a instância no gitlab, manualmente

<img src="terraform-structure/Captura de tela de 2024-04-24 23-12-45.png">

8. Executei o pipeline no gitlab

<img src="pipelines.png">

9. Deslumbrei o meu site no endereço público da instância EC2

<img src="site.png">

10. Para não perder dinheiros, executei o comando `terraform destroy` para destruir a infraestrutura
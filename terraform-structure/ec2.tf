resource "aws_instance" "web" {
  ami             = "ami-0e001c9271cf7f3b9"
  instance_type   = "t2.micro"
  key_name        = "my-key-pair"
  security_groups = ["allow_ssh", "allow_http", "allow_egress"]
  user_data       = file("script.sh")

  tags = {
    Name = "web-server"
  }
}
